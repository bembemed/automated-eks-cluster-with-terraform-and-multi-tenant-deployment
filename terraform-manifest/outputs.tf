output "location" {
  value = azurerm_resource_group.aks_rg.location
}

output "resource_group_id" {
  value = azurerm_resource_group.aks_rg.id
}

output "resource_group_name" {
  value = azurerm_resource_group.aks_rg.name
}

output "versions" {
  value = data.azurerm_kubernetes_service_versions.current.versions
}

output "latest_version" {
  value = data.azurerm_kubernetes_service_versions.current.latest_version
}

output "azure_ad_group_id" {
  value = azuread_group.aks_administrators.id
}

output "azure_ad_group_ojectid" {
  value = azuread_group.aks_administrators.object_id
}


output "aks_cluster_id" {
  value = module.aks.aks_name
}

output "aci" {
  value = module.aks.aci_connector_linux
}

output "aci_enabled" {
  value = module.aks.aci_connector_linux_enabled
}

