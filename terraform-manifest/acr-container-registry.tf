resource "azurerm_container_registry" "acr" {
  name                = var.registry_name
  location            = var.location
  resource_group_name = "${var.resource_group_name}-${var.environment}"
  sku                 = "Basic"

  depends_on = [azurerm_resource_group.aks_rg]
}

# Get the login server for the ACR
data "azurerm_container_registry" "login_server" {
  name                = azurerm_container_registry.acr.name
  resource_group_name = "${var.resource_group_name}-${var.environment}"
}

resource "azurerm_role_assignment" "acr_pull_access" {
  scope                = azurerm_container_registry.acr.id
  role_definition_name = "AcrPull"
  # principal_id         = azurerm_kubernetes_cluster.aks_cluster.kubelet_identity[0].object_id
  principal_id = module.aks.kubelet_identity[0].object_id
  depends_on   = [azurerm_container_registry.acr, module.aks]
}

