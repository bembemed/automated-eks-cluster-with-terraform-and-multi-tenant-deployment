# resource "azurerm_kubernetes_cluster" "aks_cluster" {
#   name                = "${azurerm_resource_group.aks_rg.name}-cluster"
#   location            = azurerm_resource_group.aks_rg.location
#   resource_group_name = azurerm_resource_group.aks_rg.name
#   dns_prefix          = "${azurerm_resource_group.aks_rg.name}-cluster"
#   kubernetes_version  = data.azurerm_kubernetes_service_versions.current.latest_version
#   node_resource_group = "${azurerm_resource_group.aks_rg.name}-nrg"

#   default_node_pool {
#     name                 = "systempool"
#     vm_size              = "Standard_DS2_v2"
#     orchestrator_version = data.azurerm_kubernetes_service_versions.current.latest_version
#     zones                = [3]
#     enable_auto_scaling  = true
#     max_count            = 3
#     min_count            = 1
#     os_disk_size_gb      = 30
#     type                 = "VirtualMachineScaleSets"
#     vnet_subnet_id       = azurerm_subnet.aks-defaut.id
#     node_labels = {
#       "nodepool-type" = "system"
#       "environment"   = "dev"
#       "nodepoolos"    = "linux"
#       "app"           = "system-apps"
#     }
#     tags = {
#       "nodepool-type" = "system"
#       "environment"   = "dev"
#       "nodepoolos"    = "linux"
#       "app"           = "system-apps"
#     }
#   }

#   identity {
#     type = "SystemAssigned"
#   }
#   azure_policy_enabled = true
#   oms_agent {
#     log_analytics_workspace_id = azurerm_log_analytics_workspace.insights.id

#   }

#   role_based_access_control_enabled = true
#   azure_active_directory_role_based_access_control {
#     admin_group_object_ids = [azuread_group.aks_administrators.object_id]
#     managed                = true
#   }

#   windows_profile {
#     admin_password = var.windows_admin_password
#     admin_username = var.windows_admin_username
#   }

#   linux_profile {
#     admin_username = "ubuntu"
#     ssh_key {
#       key_data = file(var.ssh_public_key)
#     }

#   }

#   network_profile {
#     network_plugin    = "azure"
#     load_balancer_sku = "standard"
#   }

#   tags = {
#     Environement = "dev"
#   }
# }

resource "azurerm_subnet" "example-aci" {
  name                 = "aks-aci-subnet"
  virtual_network_name = azurerm_virtual_network.aksvnet.name
  resource_group_name  = azurerm_resource_group.aks_rg.name
  address_prefixes     = ["10.250.0.0/16"]

  delegation {
    name = "aciDelegation"
    service_delegation {
      name    = "Microsoft.ContainerInstance/containerGroups"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}

module "aks" {
  source                          = "Azure/aks/azurerm"
  version                         = "7.5.0"
  cluster_name                    = "${azurerm_resource_group.aks_rg.name}-cluster"
  location                        = azurerm_resource_group.aks_rg.location
  resource_group_name             = azurerm_resource_group.aks_rg.name
  kubernetes_version              = data.azurerm_kubernetes_service_versions.current.latest_version
  node_resource_group             = "${azurerm_resource_group.aks_rg.name}-nrg"
  agents_pool_name                = "systempool"
  agents_size                     = "Standard_DS2_v2"
  enable_auto_scaling             = true
  orchestrator_version            = data.azurerm_kubernetes_service_versions.current.latest_version
  agents_availability_zones       = [3]
  agents_max_count                = 3
  agents_min_count                = 1
  os_disk_size_gb                 = 30
  agents_type                     = "VirtualMachineScaleSets"
  vnet_subnet_id                  = azurerm_subnet.aks-defaut.id
  aci_connector_linux_enabled     = true
  aci_connector_linux_subnet_name = azurerm_subnet.example-aci.name

  identity_type = "SystemAssigned"

  azure_policy_enabled = true
  network_plugin       = "azure"
  network_policy       = "calico"

  admin_username = "ubuntu"
  public_ssh_key = file(var.ssh_public_key)



}



resource "azurerm_role_assignment" "example" {
  scope                = azurerm_subnet.example-aci.id
  role_definition_name = "Network Contributor"
  principal_id         = module.aks.aci_connector_linux[0].connector_identity[0].object_id
}

