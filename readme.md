#  Automated EKS Cluster with Terraform and Multi-tenant Deployment

![Project Logo](images/arch-infra.png)
![Project Logo](images/archi-deploy.png)

# description

I built a Terraform project to automate the provisioning of a complete EKS (Elastic Kubernetes Service) cluster with a unique multi-tenant architecture. Here's what it achieves:

. Infrastructure as Code: Creates the EKS cluster, including Linux pools for pods and ACI connectors with virtual nodes, all within a specific virtual network using Terraform.

. Integrated Container Registry: Sets up a container registry within the virtual network and integrates it with the EKS cluster for secure image storage.

. Multi-tenant Deployment: Defines Kubernetes manifests for deploying a Java application and a MySQL database in a multi-tenant fashion:

Java app runs on ACI connectors, leveraging their serverless nature.

MySQL database runs on a dedicated Linux pool for persistence.

. Deployment Controls: Utilizes Kubernetes resources like Secrets, ConfigMaps, Persistent Volumes (Azure Disks), Persistent Volume Claims, and Services for secure and manageable deployments.

. Resource Management: Implements Resource Quotas to limit resource consumption per tenant.

